To switch the receiver to binary mode, you can use the command `gpsctl -b -D 5 -f -t "u-blox" /dev/ttyACM0`.
Here is its output:

    gpsctl:INFO: opening GPS data source type 3 at '/dev/ttyACM0'
    gpsctl:INFO: speed 38400, 8N1
    gpsctl:PROG: Probing "Garmin USB binary" driver...
    gpsctl:INFO: attempting USB device enumeration.
    gpsctl:INFO: 045e:00db (bus 2, device 4)
    gpsctl:INFO: 8087:8000 (bus 2, device 2)
    gpsctl:INFO: 1d6b:0002 (bus 2, device 1)
    gpsctl:INFO: 08bb:2902 (bus 1, device 5)
    gpsctl:INFO: 046d:c404 (bus 1, device 7)
    gpsctl:INFO: 0424:2504 (bus 1, device 4)
    gpsctl:INFO: 1546:01a7 (bus 1, device 11)
    gpsctl:INFO: 0a5c:21e8 (bus 1, device 6)
    gpsctl:INFO: 0409:005a (bus 1, device 3)
    gpsctl:INFO: 8087:8008 (bus 1, device 2)
    gpsctl:INFO: 1d6b:0002 (bus 1, device 1)
    gpsctl:INFO: 1d6b:0003 (bus 4, device 1)
    gpsctl:INFO: 1d6b:0002 (bus 3, device 1)
    gpsctl:INFO: vendor/product match with 091e:0003 not found
    gpsctl:PROG: Probe not found "Garmin USB binary" driver...
    gpsctl:PROG: Probing "GeoStar" driver...
    gpsctl:PROG: Sent GeoStar packet id 0xc1
    gpsctl:IO: => GPS: 5053474700c100010000000050924746
    gpsctl:PROG: Probe not found "GeoStar" driver...                                                                      
    gpsctl:PROG: Probing "Trimble TSIP" driver...                                                                         
    gpsctl:INFO: speed 9600, 8O1                                                                                          
    gpsctl:INFO: speed 38400, 8N1                                                                                                                                                       
    gpsctl:PROG: Probe not found "Trimble TSIP" driver...                                                                                                                               
    gpsctl:PROG: no probe matched...                                                                                                                                                    
    gpsctl:INFO: gpsd_activate(1): activated GPS (fd 3)                                                                                                                                 
    gpsctl:INFO: device /dev/ttyACM0 activated                                                                                                                                          
    gpsctl:INFO: startup at 2015-03-10T13:37:14.000Z (1425994634)                                                                                                                       
    gpsctl:PROG: switching to match packet type 1: $GPRMC,133715.00,A,5008.64927,N,00839.98328,E,0.389,,100315,,,D*7A\x0d\x0a                                                           
    gpsctl:PROG: switch_driver(NMEA0183) called...                                                                                                                                      
    gpsctl:PROG: selecting NMEA0183 driver...                                                                                                                                           
    gpsctl:INFO: /dev/ttyACM0 identified as type NMEA0183, 0.793732 sec @ 38400bps                                                                                                      
    gpsctl:PROG: => Probing for Garmin NMEA                                                                                                                                             
    gpsctl:IO: => GPS: $PGRMCE*0E\x0d\x0a                                                                                                                                               
    gpsctl:IO: <= GPS: $GPRMC,133715.00,A,5008.64927,N,00839.98328,E,0.389,,100315,,,D*7A                                                                                               
    gpsctl:PROG: GPRMC sentence timestamped 133715.00.                                                                                                                                  
    gpsctl:PROG: GPRMC starts a reporting cycle.                                                                                                                                        
    gpsctl:PROG: => Probing for SiRF                                                                                                                                                    
    gpsctl:IO: => GPS: $PSRF100,0,38400,8,1,0*3C\x0d\x0a                                                                                                                                
    gpsctl:IO: <= GPS: $GPVTG,,T,,M,0.389,N,0.721,K,D*20                                                                                                                                
    gpsctl:PROG: => Probing for FV-18                                                                                                                                                   
    gpsctl:IO: => GPS: $PFEC,GPint*58\x0d\x0a                                                                                                                                           
    gpsctl:IO: <= GPS: $GPGGA,133715.00,5008.64927,N,00839.98328,E,2,04,1.50,155.4,M,47.5,M,,0000*57                                                                                    
    gpsctl:PROG: GPGGA sentence timestamped 133715.00.                                                                                                                                  
    gpsctl:PROG: => Probing for Trimble Copernicus
    gpsctl:IO: => GPS: $PTNLSNM,0139,01*5C\x0d\x0a
    gpsctl:IO: <= GPS: $GPTXT,01,01,01,PGRM inv format*34
    gpsctl:WARN: unknown sentence: "$GPTXT,01,01,01,PGRM inv format*34\x0d\x0a"
    gpsctl:PROG: => Probing for Evermore
    gpsctl:IO: => GPS: 1002128e7f0101000101010000000000000000131003
    gpsctl:IO: <= GPS: $GPGSA,A,3,21,15,05,30,,,,,,,,,3.33,1.50,2.97*08
    gpsctl:PROG: GPGSA sets mode 3
    gpsctl:PROG: => Probing for GPSClock
    gpsctl:IO: => GPS: $PFEC,GPsrq*5B\x0d\x0a
    gpsctl:IO: <= GPS: $GPTXT,01,01,01,PSRF inv format*2B
    gpsctl:WARN: unknown sentence: "$GPTXT,01,01,01,PSRF inv format*2B\x0d\x0a"
    gpsctl:PROG: => Probing for Ashtech
    gpsctl:IO: => GPS: $PASHQ,RID*28\x0d\x0a
    gpsctl:IO: <= GPS: $GPTXT,01,01,01,PFEC inv format*2C
    gpsctl:WARN: unknown sentence: "$GPTXT,01,01,01,PFEC inv format*2C\x0d\x0a"
    gpsctl:PROG: => Probing for UBX
    gpsctl:PROG: => GPS: UBX class: 06, id: 00, len: 0, crc: 0618
    gpsctl:IO: => GPS: b562060000000618
    gpsctl:IO: <= GPS: $GPTXT,01,01,01,PTNL inv format*3A
    gpsctl:WARN: unknown sentence: "$GPTXT,01,01,01,PTNL inv format*3A\x0d\x0a"
    gpsctl:PROG: => Probing for MediaTek
    gpsctl:IO: => GPS: $PMTK605*31\x0d\x0a
    gpsctl:IO: <= GPS: $GPTXT,01,01,01,PFEC inv format*2C
    gpsctl:WARN: unknown sentence: "$GPTXT,01,01,01,PFEC inv format*2C\x0d\x0a"
    gpsctl:IO: <= GPS: $GPTXT,01,01,01,PASH inv format*36
    gpsctl:WARN: unknown sentence: "$GPTXT,01,01,01,PASH inv format*36\x0d\x0a"
    gpsctl:IO: UBX: len 28
    gpsctl:PROG: switching to match packet type 11: b56206001400030000000000000000000000070003000000000027ce
    gpsctl:PROG: switch_driver(u-blox) called...
    gpsctl:PROG: selecting u-blox driver...
    gpsctl:INFO: /dev/ttyACM0 identified as type u-blox, 0.796377 sec @ 38400bps
    gpsctl:PROG: => GPS: UBX class: 06, id: 16, len: 8, crc: 31e5
    gpsctl:IO: => GPS: b56206160800030703000000000031e5
    gpsctl:PROG: => GPS: UBX class: 0a, id: 04, len: 0, crc: 0e34
    gpsctl:IO: => GPS: b5620a0400000e34
    gpsctl:INFO: UBX_CFG_PRT: port 3
    gpsctl:IO: UBX: len 10
    gpsctl:IO: UBX: len 10
    gpsctl:IO: UBX: len 108
    gpsctl:PROG: Partial satellite data (1 of 4).
    gpsctl:PROG: /dev/ttyACM0 looks like a u-blox 1.00 (59842) at 38400.
    /dev/ttyACM0 identified as a u-blox 1.00 (59842) at 38400 baud.
    gpsctl:SHOUT: switching to mode BINARY.
    gpsctl:PROG: => GPS: UBX class: 06, id: 01, len: 3, crc: 104b
    gpsctl:IO: => GPS: b56206010300010401104b
    gpsctl:PROG: => GPS: UBX class: 06, id: 01, len: 3, crc: 124f
    gpsctl:IO: => GPS: b56206010300010601124f
    gpsctl:PROG: => GPS: UBX class: 06, id: 01, len: 3, crc: 2c83
    gpsctl:IO: => GPS: b562060103000120012c83
    gpsctl:PROG: => GPS: UBX class: 06, id: 01, len: 3, crc: 45ac
    gpsctl:IO: => GPS: b5620601030001300a45ac
    gpsctl:PROG: => GPS: UBX class: 06, id: 01, len: 3, crc: 47b0
    gpsctl:IO: => GPS: b5620601030001320a47b0
    gpsctl:PROG: => GPS: UBX class: 06, id: 00, len: 20, crc: 93ac
    gpsctl:IO: => GPS: b5620600140003000000d008000000960000070001000000000093ac
